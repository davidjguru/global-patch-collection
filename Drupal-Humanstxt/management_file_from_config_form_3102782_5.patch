diff --git a/src/Form/HumanstxtSettingsForm.php b/src/Form/HumanstxtSettingsForm.php
index a81c69d..fe3a4e2 100644
--- a/src/Form/HumanstxtSettingsForm.php
+++ b/src/Form/HumanstxtSettingsForm.php
@@ -4,6 +4,10 @@ namespace Drupal\humanstxt\Form;

 use Drupal\Core\Form\ConfigFormBase;
 use Drupal\Core\Form\FormStateInterface;
+use Drupal\Core\Logger\LoggerChannelFactoryInterface;
+use Drupal\Core\Messenger\MessengerInterface;
+use Exception;
+use Symfony\Component\DependencyInjection\ContainerInterface;

 /**
  * Implements the Humanstxt Settings form controller.
@@ -13,6 +17,42 @@ use Drupal\Core\Form\FormStateInterface;
  * @see \Drupal\Core\Form\ConfigFormBase
  */
 class HumanstxtSettingsForm extends ConfigFormBase {
+
+  /**
+   * The messenger.
+   *
+   * @var \Drupal\Core\Messenger\MessengerInterface
+   */
+  protected $messenger;
+
+  /**
+   * The logger.
+   *
+   * @var \Drupal\Core\Logger\LoggerChannelFactoryInterface
+   */
+  protected $logger;
+
+  /**
+   * Constructs a HumanstxtSettingsForm instance.
+   *
+   * @param \Drupal\Core\Messenger\MessengerInterface $messenger
+   *   The messenger.
+   * @param \Drupal\Core\Logger\LoggerChannelFactoryInterface $logger
+   *   The logger factory.
+   */
+  public function __construct(MessengerInterface $messenger,
+                              LoggerChannelFactoryInterface $logger) {
+        $this->messenger = $messenger;
+        $this->logger = $logger;
+     }
+
+  public static function create(ContainerInterface $container) {
+        return new static(
+              $container->get('messenger'),
+              $container->get('logger.factory')
+            );
+  }
+

   /**
    * Getter method for Form ID.
@@ -67,13 +107,21 @@ class HumanstxtSettingsForm extends ConfigFormBase {
                    people who have contributed to building the website, you can
                    find more info in <a href="@humanstxt">humanstxt.org</a> and
                    use <a href="@humanstxt_file">this file</a>
-                   as base file.', ['@humanstxt' => 'http://humanstxt.org',
-                   '@humanstxt_file' => 'http://humanstxt.org/humans.txt']),
+                   as base file.' . '<br>' . 'Remember that this module will try
+                   to write a new file (or edit a former version) in the Drupal
+                   base folder.' .'<br>' . 'Please, ensure this directory is
+                   writable by Drupal through the owner user of the project
+                   folder, currently: %user.' . '<br>' . 'Does this user have
+                   a relationship with the Drupal/Apache/Nginx user in your
+                   project? Please check this.' ,
+                   ['@humanstxt' => 'http://humanstxt.org',
+                   '@humanstxt_file' => 'http://humanstxt.org/humans.txt',
+                   '%user' => get_current_user()]),
     ];

     $form['humanstxt_content'] = [
       '#type' => 'textarea',
-      '#title' => $this->t('Contents of Humans.txt'),
+      '#title' => $this->t('Content of Humans.txt'),
       '#description' => $this->t('Fill the area following the pattern.'),
       '#default_value' => $content,
       '#cols' => 60,
@@ -104,14 +152,71 @@ class HumanstxtSettingsForm extends ConfigFormBase {
    * {@inheritdoc}
    */
   public function submitForm(array &$form, FormStateInterface $form_state) {
+
+    // Get the values from the form state.
+    $content_submit = $form_state->getValue('humanstxt_content');
+    $display_link_submit = $form_state->getValue('humanstxt_display_link');

-    // First set the new values in the config object of the module.
+    // Set the new values in the config object of the module.
     $this->config('humanstxt.settings')
-      ->set('content', $form_state->getValue('humanstxt_content'))
-      ->set('display_link', $form_state->getValue('humanstxt_display_link'))
+      ->set('content', $content_submit)
+      ->set('display_link', $display_link_submit)
       ->save();
-
+
+    // Call to the file processing function.
+    $this->humanstxt_processing_file($content_submit);
+
     parent::submitForm($form, $form_state);
   }

+  /**
+   * Helper function to process the Humans.txt File.
+   */
+  function humanstxt_processing_file($content) {
+        try {
+
+              // Set the complete path for the file.
+              $file_path = DRUPAL_ROOT . '/humans.txt';
+
+              // Getting the file with -w- option truncate the file to zero length.
+              // If the file does not exist attempt to create it.
+              $humanstxt_open = fopen($file_path, 'w');
+              if ( $humanstxt_open === FALSE) {
+
+                    // Failed processing the file.
+                    throw new Exception("Could not open the file!");
+               } elseif (fwrite($humanstxt_open, $content) === FALSE) {
+
+                    // Failed writing the file.
+                    throw new Exception("Could not write the file!");
+                } elseif (fclose($humanstxt_open) === FALSE) {
+
+                    // Failed closing the file.
+                    throw new Exception("Could not close the file!");
+                }
+
+    }catch (Exception $e) {
+
+      // If an error occurred we cannot save the file as linked in checkbox.
+      // Set the value in the config object of the module.
+      $this->config('humanstxt.settings')
+           ->set('display_link', 0)
+           ->save();
+
+      // Load the error message in two places.
+      $this->messenger->addError(t("It was not possible to process the
+                                   file, due to problems in permissions.
+                                   Your current User in system is: @user.
+                                   Please review the permissions or change the
+                                   current user in the project folder."
+                                    , ['@user' => get_current_user()]));
+
+      $this->logger->get('humanstxt')->error(t('Unable to write in root
+                                             folder. Review the permissions or
+                                             change the user owner (chmod,
+                                             chown). ' . $e->getMessage()));
+
+    }
+  }
+
 }
