diff --git a/src/Controller/HumansTxtController.php b/src/Controller/HumansTxtController.php
new file mode 100644
index 0000000..4740dc8
--- /dev/null
+++ b/src/Controller/HumansTxtController.php
@@ -0,0 +1,66 @@
+<?php
+
+namespace Drupal\humanstxt\Controller;
+
+use Drupal\Core\Controller\ControllerBase;
+use Symfony\Component\HttpFoundation\Response;
+use Symfony\Component\DependencyInjection\ContainerInterface;
+use Drupal\Core\DependencyInjection\ContainerInjectionInterface;
+use Drupal\Core\Config\ConfigFactoryInterface;
+use Drupal\Core\Cache\CacheableResponse;
+
+/**
+ * Class HumansTxtController.
+ * HumansTxtController is a base class for handling the humanstxt route.
+ * Returns the final humans.txt output as an object with a text/plain type.
+ *
+ * @package Drupal\humanstxt\Controller
+ * @access public
+ * @see https://www.drupal.org/project/humanstxt
+ */
+class HumansTxtController extends ControllerBase implements ContainerInjectionInterface {
+
+  /**
+   * Humanstxt module 'humanstxt.settings' configuration.
+   *
+   * @var \Drupal\Core\Config\ImmutableConfig
+   */
+  protected $humanstxtConfig;
+
+  /**
+   * Constructs a HumansTxtController object.
+   *
+   * @param \Drupal\Core\Config\ConfigFactoryInterface $config_factory
+   *   Configuration object factory.
+   */
+  public function __construct(ConfigFactoryInterface $config_factory) {
+    $this->humanstxtConfig = $config_factory->get('humanstxt.settings');
+  }
+
+  /**
+   * {@inheritdoc}
+   */
+  public static function create(ContainerInterface $container) {
+    return new static(
+      $container->get('config.factory')
+    );
+  }
+
+  /**
+   * Serves the configured humans.txt file.
+   *
+   * @return Symfony\Component\HttpFoundation\Response
+   *   The humans.txt file is a response object with 'text/plain' type.
+   */
+  public function content() {
+    // Get value from config.
+    $content = $this->humanstxtConfig->get('content');
+    // Create a new response.
+    $response = new CacheableResponse($content, Response::HTTP_OK, ['content-type' => 'text/plain']);
+    // Add cache metadata from the response.
+    $meta_data = $response->getCacheableMetadata();
+    $meta_data->addCacheTags(['humanstxt']);
+    return $response;
+  }
+
+}
